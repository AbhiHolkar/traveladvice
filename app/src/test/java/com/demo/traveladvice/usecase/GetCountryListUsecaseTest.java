package com.demo.traveladvice.usecase;

import com.demo.traveladvice.repository.CountryListRespository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import io.reactivex.Observable;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.validateMockitoUsage;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by abhiholkar on 16/09/2018.
 */
public class GetCountryListUsecaseTest {


    @Mock
    private
    CountryListRespository mockRepository;

    private GetCountryListUsecase getCountryListUsecase;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        getCountryListUsecase = new GetCountryListUsecase(mockRepository);
    }

    @After
    public void tearDown() throws Exception {
        //Added to check error
        validateMockitoUsage();
    }

    @Test
    public void getCountryModelList() throws Exception {
        //Given:
        Observable mockObservable = Mockito.mock(Observable.class);
        //When:
        when(getCountryListUsecase.getCountryModelList()).thenReturn(mockObservable);
        getCountryListUsecase.getCountryModelList();
        //Then:
        assertNotNull(mockRepository);
        verify(mockRepository).getCountryList();
    }


}