package com.demo.traveladvice.viewmodel;

import com.demo.traveladvice.model.CountryModel;
import com.demo.traveladvice.usecase.GetCountryListUsecase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Created by abhiholkar on 16/09/2018.
 */
public class CountryViewModelTest {

    @Mock
    private
    GetCountryListUsecase mockuseCase;

    private CountryViewModel countryViewModel;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        countryViewModel = new CountryViewModel(mockuseCase);

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getCountryList() throws Exception {

    }

    @Test
    public void loadCountryList() throws Exception {

        //Given :
        TestObserver<List<CountryModel>> testObserver = new TestObserver<>();
        Observable mockObservable = Mockito.mock(Observable.class);
        mockObservable.subscribe(testObserver);

        //When :
        when(mockuseCase.getCountryModelList()).thenReturn(mockObservable);
        countryViewModel.loadCountryList();

        //Then:
        assertNotNull(mockuseCase);
        verify(mockuseCase).getCountryModelList();
        testObserver.assertNoErrors();
        testObserver.onComplete();
    }


}