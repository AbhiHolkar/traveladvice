package com.demo.traveladvice.di.module;

import android.app.Application;
import android.content.Context;

import com.demo.traveladvice.network.APIService;
import com.demo.traveladvice.repository.CountryListRespository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by abhiholkar on 15/09/2018.
 */

@Module
public class AppModule {

    private Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    Context providesContext() {
        return mApplication.getApplicationContext();
    }

    @Provides
    @Singleton
    Retrofit providesRetrofit() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(APIService.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

    @Provides
    CountryListRespository providesCountryListRepository(Retrofit retrofit) {
        return new CountryListRespository(retrofit);
    }
}
