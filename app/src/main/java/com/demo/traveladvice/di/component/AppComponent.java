package com.demo.traveladvice.di.component;

import com.demo.traveladvice.di.module.AppModule;
import com.demo.traveladvice.di.module.ViewModelModule;
import com.demo.traveladvice.view.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by abhiholkar on 15/09/2018.
 */
@Singleton
@Component(modules = {AppModule.class, ViewModelModule.class})
public interface AppComponent {

    void inject(MainActivity activity);

}
