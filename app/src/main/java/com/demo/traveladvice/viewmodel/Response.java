package com.demo.traveladvice.viewmodel;

import android.support.annotation.Nullable;

import com.demo.traveladvice.model.CountryModel;

import java.util.List;

/**
 * Created by abhiholkar on 15/09/2018.
 */

public class Response {

    @Nullable
    private
    List<CountryModel> countryModelList;

    @Nullable
    private
    Throwable error;

    public Response(@Nullable List<CountryModel> countryModelList, @Nullable Throwable error) {
        this.countryModelList = countryModelList;
        this.error = error;
    }


    @Nullable
    public List<CountryModel> getCountryModelList() {
        return countryModelList;
    }

    @Nullable
    public Throwable getError() {
        return error;
    }
}
