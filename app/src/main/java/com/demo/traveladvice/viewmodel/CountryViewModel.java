package com.demo.traveladvice.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.demo.traveladvice.model.CountryModel;
import com.demo.traveladvice.usecase.GetCountryListUsecase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;

/**
 * Created by abhiholkar on 15/09/2018.
 */

public class CountryViewModel extends ViewModel {


    GetCountryListUsecase getCountryListUsecase;


    private MutableLiveData<Response> response;

    @Inject
    public CountryViewModel(GetCountryListUsecase getCountryListUsecase) {
        this.getCountryListUsecase = getCountryListUsecase;
    }

    public MutableLiveData<Response> getCountryList() {
        if (response == null) {
            response = new MutableLiveData<>();
        }
        loadCountryList();
        return response;
    }

    void loadCountryList() {


        getCountryListUsecase.getCountryModelList().subscribe(new DisposableObserver<List<CountryModel>>() {
            @Override
            public void onNext(List<CountryModel> countryModels) {

                if (!countryModels.isEmpty()) {
                    response.setValue(new Response(countryModels, null));
                } else {
                    //For now temporary resilience -- > TODO - Add customise message
                    response.setValue(new Response(null, new RuntimeException()));
                }


            }

            @Override
            public void onError(Throwable e) {
                response.setValue(new Response(null, e));

            }

            @Override
            public void onComplete() {

            }
        });

    }


}
