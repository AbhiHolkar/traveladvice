package com.demo.traveladvice.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.demo.traveladvice.R;
import com.demo.traveladvice.adapter.CountryListAdapter;
import com.demo.traveladvice.app.TravelAdviceApp;
import com.demo.traveladvice.model.CountryModel;
import com.demo.traveladvice.utils.BundleConstants;
import com.demo.traveladvice.viewmodel.CountryViewModel;
import com.demo.traveladvice.viewmodel.Response;
import com.demo.traveladvice.viewmodel.ViewModelFactory;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements OnItemClickListener<CountryModel> {

    @BindView(R.id.progress)
    ProgressBar progressBar;

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @BindView(R.id.errorview)
    TextView errorView;

    @Inject
    ViewModelFactory viewModelFactory;
    private CountryViewModel countryViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        ((TravelAdviceApp) getApplication()).getAppComponent().inject(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        countryViewModel = ViewModelProviders.of(this, viewModelFactory).get(CountryViewModel.class);
        countryViewModel.getCountryList().observe(this, response -> {
            progressBar.setVisibility(View.GONE);

            //Positive Response
            if (response.getCountryModelList() != null) {
                showData(response);
            } else {
                showError();

            }
        });


    }

    private void showError() {
        recyclerView.setVisibility(View.GONE);
        errorView.setVisibility(View.VISIBLE);
    }

    private void showData(@Nullable Response response) {
        recyclerView.setVisibility(View.VISIBLE);
        CountryListAdapter adapter = new CountryListAdapter(response.getCountryModelList(), this);
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onItemClicked(CountryModel countryModel) {
        Intent detailIntent = new Intent(this, DetailActivity.class);
        detailIntent.putExtra(BundleConstants.COUNTRY_NAME, countryModel.getName());
        detailIntent.putExtra(BundleConstants.COUNTRY_URL, countryModel.getUrl());
        startActivity(detailIntent);
    }
}
