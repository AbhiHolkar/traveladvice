package com.demo.traveladvice.view;

/**
 * Created by abhiholkar on 15/09/2018.
 */

public interface OnItemClickListener<T> {
    void onItemClicked(T item);
}
