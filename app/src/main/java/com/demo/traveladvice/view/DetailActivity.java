package com.demo.traveladvice.view;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.demo.traveladvice.R;
import com.demo.traveladvice.utils.BundleConstants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActivity extends BaseActivity {

    @BindView(R.id.webview)
    WebView webView;
    private String url;
    private String title;
    private String HOST_URL = "www.gov.uk";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        title = getIntent().getStringExtra(BundleConstants.COUNTRY_NAME);
        url = getIntent().getStringExtra(BundleConstants.COUNTRY_URL);
        setTitle(TextUtils.isEmpty(title) ? getString(R.string.app_name) : title);
        //Settings
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new CustomWebViewClient());

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (URLUtil.isNetworkUrl(url)) {
            webView.loadUrl(url);
        }
    }

    class CustomWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (Uri.parse(url).getHost().equals(HOST_URL)) {
                // This is my website, so do not override; let my WebView load the page
                return false;
            }
            // Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
            return true;
        }
    }
}
