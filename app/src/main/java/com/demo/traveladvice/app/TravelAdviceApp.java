package com.demo.traveladvice.app;

import android.app.Application;

import com.demo.traveladvice.di.component.AppComponent;
import com.demo.traveladvice.di.component.DaggerAppComponent;
import com.demo.traveladvice.di.module.AppModule;


/**
 * Created by abhiholkar on 15/09/2018.
 */

public class TravelAdviceApp extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();

    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
