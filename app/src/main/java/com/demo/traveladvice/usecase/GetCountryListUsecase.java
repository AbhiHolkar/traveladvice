package com.demo.traveladvice.usecase;

import com.demo.traveladvice.model.CountryModel;
import com.demo.traveladvice.repository.CountryListRespository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by abhiholkar on 15/09/2018.
 */

public class GetCountryListUsecase {

    private CountryListRespository respository;

    @Inject
    public GetCountryListUsecase(CountryListRespository respository) {
        this.respository = respository;
    }

    public Observable<List<CountryModel>> getCountryModelList() {
        return respository.getCountryList();
    }
}
