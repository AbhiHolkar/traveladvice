package com.demo.traveladvice.repository;

import com.demo.traveladvice.model.CountryModel;
import com.demo.traveladvice.model.data.Child;
import com.demo.traveladvice.model.data.Links;
import com.demo.traveladvice.network.APIService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

/**
 * Created by abhiholkar on 15/09/2018.
 */

public class CountryListRespository {

    private Retrofit retrofit;

    @Inject
    public CountryListRespository(Retrofit retrofit) {
        this.retrofit = retrofit;

    }

    public Observable<List<CountryModel>> getCountryList() {


        return retrofit.create(APIService.class).getCountryList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).map(foreignTravelAdviceReponse -> {
                    final Links travelLinks = foreignTravelAdviceReponse.getLinks();

                    if (foreignTravelAdviceReponse != null && travelLinks != null) {
                        List<CountryModel> countryModelList = new ArrayList<>();

                        if (!travelLinks.getChildren().isEmpty()) {
                            List<Child> childList = travelLinks.getChildren();
                            for (Child c : childList) {
                                if (c.getCountry() != null && c.getCountry().getName() != null) {
                                    CountryModel countryModel = new CountryModel(c.getCountry().getName(), c.getWebUrl());
                                    countryModelList.add(countryModel);
                                }

                            }
                            return countryModelList;
                        }

                    }
                    return Collections.emptyList();

                });

    }


}
