package com.demo.traveladvice.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.demo.traveladvice.R;
import com.demo.traveladvice.model.CountryModel;
import com.demo.traveladvice.view.OnItemClickListener;

import java.util.List;

/**
 * Created by abhiholkar on 15/09/2018.
 */

public class CountryListAdapter extends RecyclerView.Adapter<ItemViewHolder> {

    private List<CountryModel> countryList;
    private OnItemClickListener itemClickListener;

    public CountryListAdapter(List<CountryModel> countryList, OnItemClickListener itemClickListener) {
        this.countryList = countryList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, final int position) {
        holder.name.setText(countryList.get(position).getName());
        holder.name.setOnClickListener(v -> itemClickListener.onItemClicked(countryList.get(position)));

    }

    @Override
    public int getItemCount() {
        return countryList.isEmpty() ? 0 : countryList.size();
    }
}
