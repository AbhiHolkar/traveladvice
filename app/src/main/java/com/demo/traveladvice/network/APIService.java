package com.demo.traveladvice.network;

import com.demo.traveladvice.model.data.ForeignTravelAdviceReponse;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by abhiholkar on 15/09/2018.
 */

public interface APIService {

    String BASE_URL = "https://www.gov.uk/api/";

    @GET("content/foreign-travel-advice")
    Observable<ForeignTravelAdviceReponse> getCountryList();
}
