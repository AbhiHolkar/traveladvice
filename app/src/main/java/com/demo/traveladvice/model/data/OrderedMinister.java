package com.demo.traveladvice.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class OrderedMinister {

    @SerializedName("attends_cabinet_type")
    private Object attendsCabinetType;
    @Expose
    private String href;
    @Expose
    private Image image;
    @Expose
    private String name;
    @SerializedName("name_prefix")
    private String namePrefix;
    @SerializedName("payment_type")
    private Object paymentType;
    @Expose
    private String role;
    @SerializedName("role_href")
    private String roleHref;

    public Object getAttendsCabinetType() {
        return attendsCabinetType;
    }

    public String getHref() {
        return href;
    }

    public Image getImage() {
        return image;
    }

    public String getName() {
        return name;
    }

    public String getNamePrefix() {
        return namePrefix;
    }

    public Object getPaymentType() {
        return paymentType;
    }

    public String getRole() {
        return role;
    }

    public String getRoleHref() {
        return roleHref;
    }

    public static class Builder {

        private Object attendsCabinetType;
        private String href;
        private Image image;
        private String name;
        private String namePrefix;
        private Object paymentType;
        private String role;
        private String roleHref;

        public OrderedMinister.Builder withAttendsCabinetType(Object attendsCabinetType) {
            this.attendsCabinetType = attendsCabinetType;
            return this;
        }

        public OrderedMinister.Builder withHref(String href) {
            this.href = href;
            return this;
        }

        public OrderedMinister.Builder withImage(Image image) {
            this.image = image;
            return this;
        }

        public OrderedMinister.Builder withName(String name) {
            this.name = name;
            return this;
        }

        public OrderedMinister.Builder withNamePrefix(String namePrefix) {
            this.namePrefix = namePrefix;
            return this;
        }

        public OrderedMinister.Builder withPaymentType(Object paymentType) {
            this.paymentType = paymentType;
            return this;
        }

        public OrderedMinister.Builder withRole(String role) {
            this.role = role;
            return this;
        }

        public OrderedMinister.Builder withRoleHref(String roleHref) {
            this.roleHref = roleHref;
            return this;
        }

        public OrderedMinister build() {
            OrderedMinister orderedMinister = new OrderedMinister();
            orderedMinister.attendsCabinetType = attendsCabinetType;
            orderedMinister.href = href;
            orderedMinister.image = image;
            orderedMinister.name = name;
            orderedMinister.namePrefix = namePrefix;
            orderedMinister.paymentType = paymentType;
            orderedMinister.role = role;
            orderedMinister.roleHref = roleHref;
            return orderedMinister;
        }

    }

}
