package com.demo.traveladvice.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class PrimaryPublishingOrganisation {

    @SerializedName("analytics_identifier")
    private String analyticsIdentifier;
    @SerializedName("api_path")
    private String apiPath;
    @SerializedName("api_url")
    private String apiUrl;
    @SerializedName("base_path")
    private String basePath;
    @SerializedName("content_id")
    private String contentId;
    @Expose
    private Details details;
    @SerializedName("document_type")
    private String documentType;
    @Expose
    private Links links;
    @Expose
    private String locale;
    @SerializedName("public_updated_at")
    private String publicUpdatedAt;
    @SerializedName("schema_name")
    private String schemaName;
    @Expose
    private String title;
    @SerializedName("web_url")
    private String webUrl;
    @Expose
    private Boolean withdrawn;

    public String getAnalyticsIdentifier() {
        return analyticsIdentifier;
    }

    public String getApiPath() {
        return apiPath;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public String getBasePath() {
        return basePath;
    }

    public String getContentId() {
        return contentId;
    }

    public Details getDetails() {
        return details;
    }

    public String getDocumentType() {
        return documentType;
    }

    public Links getLinks() {
        return links;
    }

    public String getLocale() {
        return locale;
    }

    public String getPublicUpdatedAt() {
        return publicUpdatedAt;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public String getTitle() {
        return title;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public Boolean getWithdrawn() {
        return withdrawn;
    }

    public static class Builder {

        private String analyticsIdentifier;
        private String apiPath;
        private String apiUrl;
        private String basePath;
        private String contentId;
        private Details details;
        private String documentType;
        private Links links;
        private String locale;
        private String publicUpdatedAt;
        private String schemaName;
        private String title;
        private String webUrl;
        private Boolean withdrawn;

        public PrimaryPublishingOrganisation.Builder withAnalyticsIdentifier(String analyticsIdentifier) {
            this.analyticsIdentifier = analyticsIdentifier;
            return this;
        }

        public PrimaryPublishingOrganisation.Builder withApiPath(String apiPath) {
            this.apiPath = apiPath;
            return this;
        }

        public PrimaryPublishingOrganisation.Builder withApiUrl(String apiUrl) {
            this.apiUrl = apiUrl;
            return this;
        }

        public PrimaryPublishingOrganisation.Builder withBasePath(String basePath) {
            this.basePath = basePath;
            return this;
        }

        public PrimaryPublishingOrganisation.Builder withContentId(String contentId) {
            this.contentId = contentId;
            return this;
        }

        public PrimaryPublishingOrganisation.Builder withDetails(Details details) {
            this.details = details;
            return this;
        }

        public PrimaryPublishingOrganisation.Builder withDocumentType(String documentType) {
            this.documentType = documentType;
            return this;
        }

        public PrimaryPublishingOrganisation.Builder withLinks(Links links) {
            this.links = links;
            return this;
        }

        public PrimaryPublishingOrganisation.Builder withLocale(String locale) {
            this.locale = locale;
            return this;
        }

        public PrimaryPublishingOrganisation.Builder withPublicUpdatedAt(String publicUpdatedAt) {
            this.publicUpdatedAt = publicUpdatedAt;
            return this;
        }

        public PrimaryPublishingOrganisation.Builder withSchemaName(String schemaName) {
            this.schemaName = schemaName;
            return this;
        }

        public PrimaryPublishingOrganisation.Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public PrimaryPublishingOrganisation.Builder withWebUrl(String webUrl) {
            this.webUrl = webUrl;
            return this;
        }

        public PrimaryPublishingOrganisation.Builder withWithdrawn(Boolean withdrawn) {
            this.withdrawn = withdrawn;
            return this;
        }

        public PrimaryPublishingOrganisation build() {
            PrimaryPublishingOrganisation primaryPublishingOrganisation = new PrimaryPublishingOrganisation();
            primaryPublishingOrganisation.analyticsIdentifier = analyticsIdentifier;
            primaryPublishingOrganisation.apiPath = apiPath;
            primaryPublishingOrganisation.apiUrl = apiUrl;
            primaryPublishingOrganisation.basePath = basePath;
            primaryPublishingOrganisation.contentId = contentId;
            primaryPublishingOrganisation.details = details;
            primaryPublishingOrganisation.documentType = documentType;
            primaryPublishingOrganisation.links = links;
            primaryPublishingOrganisation.locale = locale;
            primaryPublishingOrganisation.publicUpdatedAt = publicUpdatedAt;
            primaryPublishingOrganisation.schemaName = schemaName;
            primaryPublishingOrganisation.title = title;
            primaryPublishingOrganisation.webUrl = webUrl;
            primaryPublishingOrganisation.withdrawn = withdrawn;
            return primaryPublishingOrganisation;
        }

    }

}
