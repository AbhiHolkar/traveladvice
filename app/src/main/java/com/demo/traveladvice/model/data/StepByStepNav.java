package com.demo.traveladvice.model.data;

import com.google.gson.annotations.Expose;

import java.util.List;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class StepByStepNav {

    @Expose
    private List<Introduction> introduction;
    @Expose
    private List<Step> steps;
    @Expose
    private String title;

    public List<Introduction> getIntroduction() {
        return introduction;
    }

    public List<Step> getSteps() {
        return steps;
    }

    public String getTitle() {
        return title;
    }

    public static class Builder {

        private List<Introduction> introduction;
        private List<Step> steps;
        private String title;

        public StepByStepNav.Builder withIntroduction(List<Introduction> introduction) {
            this.introduction = introduction;
            return this;
        }

        public StepByStepNav.Builder withSteps(List<Step> steps) {
            this.steps = steps;
            return this;
        }

        public StepByStepNav.Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public StepByStepNav build() {
            StepByStepNav stepByStepNav = new StepByStepNav();
            stepByStepNav.introduction = introduction;
            stepByStepNav.steps = steps;
            stepByStepNav.title = title;
            return stepByStepNav;
        }

    }

}
