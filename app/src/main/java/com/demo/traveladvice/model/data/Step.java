package com.demo.traveladvice.model.data;

import com.google.gson.annotations.Expose;

import java.util.List;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Step {

    @Expose
    private List<Content> contents;
    @Expose
    private Boolean optional;
    @Expose
    private String title;

    public List<Content> getContents() {
        return contents;
    }

    public Boolean getOptional() {
        return optional;
    }

    public String getTitle() {
        return title;
    }

    public static class Builder {

        private List<Content> contents;
        private Boolean optional;
        private String title;

        public Step.Builder withContents(List<Content> contents) {
            this.contents = contents;
            return this;
        }

        public Step.Builder withOptional(Boolean optional) {
            this.optional = optional;
            return this;
        }

        public Step.Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public Step build() {
            Step step = new Step();
            step.contents = contents;
            step.optional = optional;
            step.title = title;
            return step;
        }

    }

}
