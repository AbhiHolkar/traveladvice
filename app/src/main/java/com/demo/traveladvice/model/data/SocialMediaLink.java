package com.demo.traveladvice.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class SocialMediaLink {

    @Expose
    private String href;
    @SerializedName("service_type")
    private String serviceType;
    @Expose
    private String title;

    public String getHref() {
        return href;
    }

    public String getServiceType() {
        return serviceType;
    }

    public String getTitle() {
        return title;
    }

    public static class Builder {

        private String href;
        private String serviceType;
        private String title;

        public SocialMediaLink.Builder withHref(String href) {
            this.href = href;
            return this;
        }

        public SocialMediaLink.Builder withServiceType(String serviceType) {
            this.serviceType = serviceType;
            return this;
        }

        public SocialMediaLink.Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public SocialMediaLink build() {
            SocialMediaLink socialMediaLink = new SocialMediaLink();
            socialMediaLink.href = href;
            socialMediaLink.serviceType = serviceType;
            socialMediaLink.title = title;
            return socialMediaLink;
        }

    }

}
