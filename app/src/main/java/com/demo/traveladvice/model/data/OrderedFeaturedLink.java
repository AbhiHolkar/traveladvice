package com.demo.traveladvice.model.data;

import com.google.gson.annotations.Expose;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class OrderedFeaturedLink {

    @Expose
    private String href;
    @Expose
    private String title;

    public String getHref() {
        return href;
    }

    public String getTitle() {
        return title;
    }

    public static class Builder {

        private String href;
        private String title;

        public OrderedFeaturedLink.Builder withHref(String href) {
            this.href = href;
            return this;
        }

        public OrderedFeaturedLink.Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public OrderedFeaturedLink build() {
            OrderedFeaturedLink orderedFeaturedLink = new OrderedFeaturedLink();
            orderedFeaturedLink.href = href;
            orderedFeaturedLink.title = title;
            return orderedFeaturedLink;
        }

    }

}
