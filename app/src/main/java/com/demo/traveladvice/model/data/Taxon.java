package com.demo.traveladvice.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Taxon {

    @SerializedName("api_path")
    private String apiPath;
    @SerializedName("api_url")
    private String apiUrl;
    @SerializedName("base_path")
    private String basePath;
    @SerializedName("content_id")
    private String contentId;
    @Expose
    private Details details;
    @SerializedName("document_type")
    private String documentType;
    @Expose
    private Links links;
    @Expose
    private String locale;
    @Expose
    private String phase;
    @SerializedName("public_updated_at")
    private String publicUpdatedAt;
    @SerializedName("schema_name")
    private String schemaName;
    @Expose
    private String title;
    @SerializedName("web_url")
    private String webUrl;
    @Expose
    private Boolean withdrawn;

    public String getApiPath() {
        return apiPath;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public String getBasePath() {
        return basePath;
    }

    public String getContentId() {
        return contentId;
    }

    public Details getDetails() {
        return details;
    }

    public String getDocumentType() {
        return documentType;
    }

    public Links getLinks() {
        return links;
    }

    public String getLocale() {
        return locale;
    }

    public String getPhase() {
        return phase;
    }

    public String getPublicUpdatedAt() {
        return publicUpdatedAt;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public String getTitle() {
        return title;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public Boolean getWithdrawn() {
        return withdrawn;
    }

    public static class Builder {

        private String apiPath;
        private String apiUrl;
        private String basePath;
        private String contentId;
        private Details details;
        private String documentType;
        private Links links;
        private String locale;
        private String phase;
        private String publicUpdatedAt;
        private String schemaName;
        private String title;
        private String webUrl;
        private Boolean withdrawn;

        public Taxon.Builder withApiPath(String apiPath) {
            this.apiPath = apiPath;
            return this;
        }

        public Taxon.Builder withApiUrl(String apiUrl) {
            this.apiUrl = apiUrl;
            return this;
        }

        public Taxon.Builder withBasePath(String basePath) {
            this.basePath = basePath;
            return this;
        }

        public Taxon.Builder withContentId(String contentId) {
            this.contentId = contentId;
            return this;
        }

        public Taxon.Builder withDetails(Details details) {
            this.details = details;
            return this;
        }

        public Taxon.Builder withDocumentType(String documentType) {
            this.documentType = documentType;
            return this;
        }

        public Taxon.Builder withLinks(Links links) {
            this.links = links;
            return this;
        }

        public Taxon.Builder withLocale(String locale) {
            this.locale = locale;
            return this;
        }

        public Taxon.Builder withPhase(String phase) {
            this.phase = phase;
            return this;
        }

        public Taxon.Builder withPublicUpdatedAt(String publicUpdatedAt) {
            this.publicUpdatedAt = publicUpdatedAt;
            return this;
        }

        public Taxon.Builder withSchemaName(String schemaName) {
            this.schemaName = schemaName;
            return this;
        }

        public Taxon.Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public Taxon.Builder withWebUrl(String webUrl) {
            this.webUrl = webUrl;
            return this;
        }

        public Taxon.Builder withWithdrawn(Boolean withdrawn) {
            this.withdrawn = withdrawn;
            return this;
        }

        public Taxon build() {
            Taxon taxon = new Taxon();
            taxon.apiPath = apiPath;
            taxon.apiUrl = apiUrl;
            taxon.basePath = basePath;
            taxon.contentId = contentId;
            taxon.details = details;
            taxon.documentType = documentType;
            taxon.links = links;
            taxon.locale = locale;
            taxon.phase = phase;
            taxon.publicUpdatedAt = publicUpdatedAt;
            taxon.schemaName = schemaName;
            taxon.title = title;
            taxon.webUrl = webUrl;
            taxon.withdrawn = withdrawn;
            return taxon;
        }

    }

}
