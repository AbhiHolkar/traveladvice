package com.demo.traveladvice.model;

/**
 * Created by abhiholkar on 15/09/2018.
 */

public class CountryModel {

    private String name;
    private String url;


    public CountryModel(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }
}
