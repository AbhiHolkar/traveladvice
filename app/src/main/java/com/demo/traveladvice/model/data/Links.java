package com.demo.traveladvice.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Links {

    @SerializedName("available_translations")
    private List<AvailableTranslation> availableTranslations;
    @Expose
    private List<Child> children;
    @SerializedName("mainstream_browse_pages")
    private List<MainstreamBrowsePage> mainstreamBrowsePages;
    @SerializedName("ordered_related_items")
    private List<OrderedRelatedItem> orderedRelatedItems;
    @SerializedName("pages_related_to_step_nav")
    private List<PagesRelatedToStepNav> pagesRelatedToStepNav;
    @Expose
    private List<Parent> parent;
    @SerializedName("parent_taxons")
    private List<ParentTaxon> parentTaxons;
    @SerializedName("primary_publishing_organisation")
    private List<PrimaryPublishingOrganisation> primaryPublishingOrganisation;
    @SerializedName("related_to_step_navs")
    private List<RelatedToStepNav> relatedToStepNavs;
    @SerializedName("root_taxon")
    private List<RootTaxon> rootTaxon;
    @Expose
    private List<Taxon> taxons;

    public List<AvailableTranslation> getAvailableTranslations() {
        return availableTranslations;
    }

    public List<Child> getChildren() {
        return children;
    }

    public List<MainstreamBrowsePage> getMainstreamBrowsePages() {
        return mainstreamBrowsePages;
    }

    public List<OrderedRelatedItem> getOrderedRelatedItems() {
        return orderedRelatedItems;
    }

    public List<PagesRelatedToStepNav> getPagesRelatedToStepNav() {
        return pagesRelatedToStepNav;
    }

    public List<Parent> getParent() {
        return parent;
    }

    public List<ParentTaxon> getParentTaxons() {
        return parentTaxons;
    }

    public List<PrimaryPublishingOrganisation> getPrimaryPublishingOrganisation() {
        return primaryPublishingOrganisation;
    }

    public List<RelatedToStepNav> getRelatedToStepNavs() {
        return relatedToStepNavs;
    }

    public List<RootTaxon> getRootTaxon() {
        return rootTaxon;
    }

    public List<Taxon> getTaxons() {
        return taxons;
    }

    public static class Builder {

        private List<AvailableTranslation> availableTranslations;
        private List<Child> children;
        private List<MainstreamBrowsePage> mainstreamBrowsePages;
        private List<OrderedRelatedItem> orderedRelatedItems;
        private List<PagesRelatedToStepNav> pagesRelatedToStepNav;
        private List<Parent> parent;
        private List<ParentTaxon> parentTaxons;
        private List<PrimaryPublishingOrganisation> primaryPublishingOrganisation;
        private List<RelatedToStepNav> relatedToStepNavs;
        private List<RootTaxon> rootTaxon;
        private List<Taxon> taxons;

        public Links.Builder withAvailableTranslations(List<AvailableTranslation> availableTranslations) {
            this.availableTranslations = availableTranslations;
            return this;
        }

        public Links.Builder withChildren(List<Child> children) {
            this.children = children;
            return this;
        }

        public Links.Builder withMainstreamBrowsePages(List<MainstreamBrowsePage> mainstreamBrowsePages) {
            this.mainstreamBrowsePages = mainstreamBrowsePages;
            return this;
        }

        public Links.Builder withOrderedRelatedItems(List<OrderedRelatedItem> orderedRelatedItems) {
            this.orderedRelatedItems = orderedRelatedItems;
            return this;
        }

        public Links.Builder withPagesRelatedToStepNav(List<PagesRelatedToStepNav> pagesRelatedToStepNav) {
            this.pagesRelatedToStepNav = pagesRelatedToStepNav;
            return this;
        }

        public Links.Builder withParent(List<Parent> parent) {
            this.parent = parent;
            return this;
        }

        public Links.Builder withParentTaxons(List<ParentTaxon> parentTaxons) {
            this.parentTaxons = parentTaxons;
            return this;
        }

        public Links.Builder withPrimaryPublishingOrganisation(List<PrimaryPublishingOrganisation> primaryPublishingOrganisation) {
            this.primaryPublishingOrganisation = primaryPublishingOrganisation;
            return this;
        }

        public Links.Builder withRelatedToStepNavs(List<RelatedToStepNav> relatedToStepNavs) {
            this.relatedToStepNavs = relatedToStepNavs;
            return this;
        }

        public Links.Builder withRootTaxon(List<RootTaxon> rootTaxon) {
            this.rootTaxon = rootTaxon;
            return this;
        }

        public Links.Builder withTaxons(List<Taxon> taxons) {
            this.taxons = taxons;
            return this;
        }

        public Links build() {
            Links links = new Links();
            links.availableTranslations = availableTranslations;
            links.children = children;
            links.mainstreamBrowsePages = mainstreamBrowsePages;
            links.orderedRelatedItems = orderedRelatedItems;
            links.pagesRelatedToStepNav = pagesRelatedToStepNav;
            links.parent = parent;
            links.parentTaxons = parentTaxons;
            links.primaryPublishingOrganisation = primaryPublishingOrganisation;
            links.relatedToStepNavs = relatedToStepNavs;
            links.rootTaxon = rootTaxon;
            links.taxons = taxons;
            return links;
        }

    }

}
