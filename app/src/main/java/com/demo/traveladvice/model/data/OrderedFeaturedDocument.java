package com.demo.traveladvice.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class OrderedFeaturedDocument {

    @SerializedName("document_type")
    private String documentType;
    @Expose
    private String href;
    @Expose
    private Image image;
    @SerializedName("public_updated_at")
    private String publicUpdatedAt;
    @Expose
    private String summary;
    @Expose
    private String title;

    public String getDocumentType() {
        return documentType;
    }

    public String getHref() {
        return href;
    }

    public Image getImage() {
        return image;
    }

    public String getPublicUpdatedAt() {
        return publicUpdatedAt;
    }

    public String getSummary() {
        return summary;
    }

    public String getTitle() {
        return title;
    }

    public static class Builder {

        private String documentType;
        private String href;
        private Image image;
        private String publicUpdatedAt;
        private String summary;
        private String title;

        public OrderedFeaturedDocument.Builder withDocumentType(String documentType) {
            this.documentType = documentType;
            return this;
        }

        public OrderedFeaturedDocument.Builder withHref(String href) {
            this.href = href;
            return this;
        }

        public OrderedFeaturedDocument.Builder withImage(Image image) {
            this.image = image;
            return this;
        }

        public OrderedFeaturedDocument.Builder withPublicUpdatedAt(String publicUpdatedAt) {
            this.publicUpdatedAt = publicUpdatedAt;
            return this;
        }

        public OrderedFeaturedDocument.Builder withSummary(String summary) {
            this.summary = summary;
            return this;
        }

        public OrderedFeaturedDocument.Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public OrderedFeaturedDocument build() {
            OrderedFeaturedDocument orderedFeaturedDocument = new OrderedFeaturedDocument();
            orderedFeaturedDocument.documentType = documentType;
            orderedFeaturedDocument.href = href;
            orderedFeaturedDocument.image = image;
            orderedFeaturedDocument.publicUpdatedAt = publicUpdatedAt;
            orderedFeaturedDocument.summary = summary;
            orderedFeaturedDocument.title = title;
            return orderedFeaturedDocument;
        }

    }

}
