package com.demo.traveladvice.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Image {

    @SerializedName("alt_text")
    private String altText;
    @Expose
    private String url;

    public String getAltText() {
        return altText;
    }

    public String getUrl() {
        return url;
    }

    public static class Builder {

        private String altText;
        private String url;

        public Image.Builder withAltText(String altText) {
            this.altText = altText;
            return this;
        }

        public Image.Builder withUrl(String url) {
            this.url = url;
            return this;
        }

        public Image build() {
            Image image = new Image();
            image.altText = altText;
            image.url = url;
            return image;
        }

    }

}
