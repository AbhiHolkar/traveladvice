package com.demo.traveladvice.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ForeignTravelAdviceReponse {

    @SerializedName("analytics_identifier")
    private Object analyticsIdentifier;
    @SerializedName("base_path")
    private String basePath;
    @SerializedName("content_id")
    private String contentId;
    @SerializedName("content_purpose_document_supertype")
    private String contentPurposeDocumentSupertype;
    @SerializedName("content_purpose_subgroup")
    private String contentPurposeSubgroup;
    @SerializedName("content_purpose_supergroup")
    private String contentPurposeSupergroup;
    @Expose
    private String description;
    @Expose
    private Details details;
    @SerializedName("document_type")
    private String documentType;
    @SerializedName("email_document_supertype")
    private String emailDocumentSupertype;
    @SerializedName("first_published_at")
    private String firstPublishedAt;
    @SerializedName("government_document_supertype")
    private String governmentDocumentSupertype;
    @Expose
    private Links links;
    @Expose
    private String locale;
    @SerializedName("navigation_document_supertype")
    private String navigationDocumentSupertype;
    @Expose
    private String phase;
    @SerializedName("public_updated_at")
    private String publicUpdatedAt;
    @SerializedName("publishing_app")
    private String publishingApp;
    @SerializedName("publishing_request_id")
    private String publishingRequestId;
    @SerializedName("publishing_scheduled_at")
    private Object publishingScheduledAt;
    @SerializedName("rendering_app")
    private String renderingApp;
    @SerializedName("scheduled_publishing_delay_seconds")
    private Object scheduledPublishingDelaySeconds;
    @SerializedName("schema_name")
    private String schemaName;
    @SerializedName("search_user_need_document_supertype")
    private String searchUserNeedDocumentSupertype;
    @Expose
    private String title;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("user_journey_document_supertype")
    private String userJourneyDocumentSupertype;
    @SerializedName("withdrawn_notice")
    private WithdrawnNotice withdrawnNotice;

    public Object getAnalyticsIdentifier() {
        return analyticsIdentifier;
    }

    public String getBasePath() {
        return basePath;
    }

    public String getContentId() {
        return contentId;
    }

    public String getContentPurposeDocumentSupertype() {
        return contentPurposeDocumentSupertype;
    }

    public String getContentPurposeSubgroup() {
        return contentPurposeSubgroup;
    }

    public String getContentPurposeSupergroup() {
        return contentPurposeSupergroup;
    }

    public String getDescription() {
        return description;
    }

    public Details getDetails() {
        return details;
    }

    public String getDocumentType() {
        return documentType;
    }

    public String getEmailDocumentSupertype() {
        return emailDocumentSupertype;
    }

    public String getFirstPublishedAt() {
        return firstPublishedAt;
    }

    public String getGovernmentDocumentSupertype() {
        return governmentDocumentSupertype;
    }

    public Links getLinks() {
        return links;
    }

    public String getLocale() {
        return locale;
    }

    public String getNavigationDocumentSupertype() {
        return navigationDocumentSupertype;
    }

    public String getPhase() {
        return phase;
    }

    public String getPublicUpdatedAt() {
        return publicUpdatedAt;
    }

    public String getPublishingApp() {
        return publishingApp;
    }

    public String getPublishingRequestId() {
        return publishingRequestId;
    }

    public Object getPublishingScheduledAt() {
        return publishingScheduledAt;
    }

    public String getRenderingApp() {
        return renderingApp;
    }

    public Object getScheduledPublishingDelaySeconds() {
        return scheduledPublishingDelaySeconds;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public String getSearchUserNeedDocumentSupertype() {
        return searchUserNeedDocumentSupertype;
    }

    public String getTitle() {
        return title;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getUserJourneyDocumentSupertype() {
        return userJourneyDocumentSupertype;
    }

    public WithdrawnNotice getWithdrawnNotice() {
        return withdrawnNotice;
    }

    public static class Builder {

        private Object analyticsIdentifier;
        private String basePath;
        private String contentId;
        private String contentPurposeDocumentSupertype;
        private String contentPurposeSubgroup;
        private String contentPurposeSupergroup;
        private String description;
        private Details details;
        private String documentType;
        private String emailDocumentSupertype;
        private String firstPublishedAt;
        private String governmentDocumentSupertype;
        private Links links;
        private String locale;
        private String navigationDocumentSupertype;
        private String phase;
        private String publicUpdatedAt;
        private String publishingApp;
        private String publishingRequestId;
        private Object publishingScheduledAt;
        private String renderingApp;
        private Object scheduledPublishingDelaySeconds;
        private String schemaName;
        private String searchUserNeedDocumentSupertype;
        private String title;
        private String updatedAt;
        private String userJourneyDocumentSupertype;
        private WithdrawnNotice withdrawnNotice;

        public ForeignTravelAdviceReponse.Builder withAnalyticsIdentifier(Object analyticsIdentifier) {
            this.analyticsIdentifier = analyticsIdentifier;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withBasePath(String basePath) {
            this.basePath = basePath;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withContentId(String contentId) {
            this.contentId = contentId;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withContentPurposeDocumentSupertype(String contentPurposeDocumentSupertype) {
            this.contentPurposeDocumentSupertype = contentPurposeDocumentSupertype;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withContentPurposeSubgroup(String contentPurposeSubgroup) {
            this.contentPurposeSubgroup = contentPurposeSubgroup;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withContentPurposeSupergroup(String contentPurposeSupergroup) {
            this.contentPurposeSupergroup = contentPurposeSupergroup;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withDetails(Details details) {
            this.details = details;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withDocumentType(String documentType) {
            this.documentType = documentType;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withEmailDocumentSupertype(String emailDocumentSupertype) {
            this.emailDocumentSupertype = emailDocumentSupertype;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withFirstPublishedAt(String firstPublishedAt) {
            this.firstPublishedAt = firstPublishedAt;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withGovernmentDocumentSupertype(String governmentDocumentSupertype) {
            this.governmentDocumentSupertype = governmentDocumentSupertype;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withLinks(Links links) {
            this.links = links;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withLocale(String locale) {
            this.locale = locale;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withNavigationDocumentSupertype(String navigationDocumentSupertype) {
            this.navigationDocumentSupertype = navigationDocumentSupertype;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withPhase(String phase) {
            this.phase = phase;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withPublicUpdatedAt(String publicUpdatedAt) {
            this.publicUpdatedAt = publicUpdatedAt;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withPublishingApp(String publishingApp) {
            this.publishingApp = publishingApp;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withPublishingRequestId(String publishingRequestId) {
            this.publishingRequestId = publishingRequestId;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withPublishingScheduledAt(Object publishingScheduledAt) {
            this.publishingScheduledAt = publishingScheduledAt;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withRenderingApp(String renderingApp) {
            this.renderingApp = renderingApp;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withScheduledPublishingDelaySeconds(Object scheduledPublishingDelaySeconds) {
            this.scheduledPublishingDelaySeconds = scheduledPublishingDelaySeconds;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withSchemaName(String schemaName) {
            this.schemaName = schemaName;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withSearchUserNeedDocumentSupertype(String searchUserNeedDocumentSupertype) {
            this.searchUserNeedDocumentSupertype = searchUserNeedDocumentSupertype;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withUserJourneyDocumentSupertype(String userJourneyDocumentSupertype) {
            this.userJourneyDocumentSupertype = userJourneyDocumentSupertype;
            return this;
        }

        public ForeignTravelAdviceReponse.Builder withWithdrawnNotice(WithdrawnNotice withdrawnNotice) {
            this.withdrawnNotice = withdrawnNotice;
            return this;
        }

        public ForeignTravelAdviceReponse build() {
            ForeignTravelAdviceReponse foreignTravelAdviceReponse = new ForeignTravelAdviceReponse();
            foreignTravelAdviceReponse.analyticsIdentifier = analyticsIdentifier;
            foreignTravelAdviceReponse.basePath = basePath;
            foreignTravelAdviceReponse.contentId = contentId;
            foreignTravelAdviceReponse.contentPurposeDocumentSupertype = contentPurposeDocumentSupertype;
            foreignTravelAdviceReponse.contentPurposeSubgroup = contentPurposeSubgroup;
            foreignTravelAdviceReponse.contentPurposeSupergroup = contentPurposeSupergroup;
            foreignTravelAdviceReponse.description = description;
            foreignTravelAdviceReponse.details = details;
            foreignTravelAdviceReponse.documentType = documentType;
            foreignTravelAdviceReponse.emailDocumentSupertype = emailDocumentSupertype;
            foreignTravelAdviceReponse.firstPublishedAt = firstPublishedAt;
            foreignTravelAdviceReponse.governmentDocumentSupertype = governmentDocumentSupertype;
            foreignTravelAdviceReponse.links = links;
            foreignTravelAdviceReponse.locale = locale;
            foreignTravelAdviceReponse.navigationDocumentSupertype = navigationDocumentSupertype;
            foreignTravelAdviceReponse.phase = phase;
            foreignTravelAdviceReponse.publicUpdatedAt = publicUpdatedAt;
            foreignTravelAdviceReponse.publishingApp = publishingApp;
            foreignTravelAdviceReponse.publishingRequestId = publishingRequestId;
            foreignTravelAdviceReponse.publishingScheduledAt = publishingScheduledAt;
            foreignTravelAdviceReponse.renderingApp = renderingApp;
            foreignTravelAdviceReponse.scheduledPublishingDelaySeconds = scheduledPublishingDelaySeconds;
            foreignTravelAdviceReponse.schemaName = schemaName;
            foreignTravelAdviceReponse.searchUserNeedDocumentSupertype = searchUserNeedDocumentSupertype;
            foreignTravelAdviceReponse.title = title;
            foreignTravelAdviceReponse.updatedAt = updatedAt;
            foreignTravelAdviceReponse.userJourneyDocumentSupertype = userJourneyDocumentSupertype;
            foreignTravelAdviceReponse.withdrawnNotice = withdrawnNotice;
            return foreignTravelAdviceReponse;
        }

    }

}
