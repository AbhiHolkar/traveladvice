package com.demo.traveladvice.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class OrderedBoardMember {

    @SerializedName("attends_cabinet_type")
    private Object attendsCabinetType;
    @Expose
    private String href;
    @Expose
    private Image image;
    @Expose
    private String name;
    @SerializedName("name_prefix")
    private Object namePrefix;
    @SerializedName("payment_type")
    private Object paymentType;
    @Expose
    private String role;
    @SerializedName("role_href")
    private Object roleHref;

    public Object getAttendsCabinetType() {
        return attendsCabinetType;
    }

    public String getHref() {
        return href;
    }

    public Image getImage() {
        return image;
    }

    public String getName() {
        return name;
    }

    public Object getNamePrefix() {
        return namePrefix;
    }

    public Object getPaymentType() {
        return paymentType;
    }

    public String getRole() {
        return role;
    }

    public Object getRoleHref() {
        return roleHref;
    }

    public static class Builder {

        private Object attendsCabinetType;
        private String href;
        private Image image;
        private String name;
        private Object namePrefix;
        private Object paymentType;
        private String role;
        private Object roleHref;

        public OrderedBoardMember.Builder withAttendsCabinetType(Object attendsCabinetType) {
            this.attendsCabinetType = attendsCabinetType;
            return this;
        }

        public OrderedBoardMember.Builder withHref(String href) {
            this.href = href;
            return this;
        }

        public OrderedBoardMember.Builder withImage(Image image) {
            this.image = image;
            return this;
        }

        public OrderedBoardMember.Builder withName(String name) {
            this.name = name;
            return this;
        }

        public OrderedBoardMember.Builder withNamePrefix(Object namePrefix) {
            this.namePrefix = namePrefix;
            return this;
        }

        public OrderedBoardMember.Builder withPaymentType(Object paymentType) {
            this.paymentType = paymentType;
            return this;
        }

        public OrderedBoardMember.Builder withRole(String role) {
            this.role = role;
            return this;
        }

        public OrderedBoardMember.Builder withRoleHref(Object roleHref) {
            this.roleHref = roleHref;
            return this;
        }

        public OrderedBoardMember build() {
            OrderedBoardMember orderedBoardMember = new OrderedBoardMember();
            orderedBoardMember.attendsCabinetType = attendsCabinetType;
            orderedBoardMember.href = href;
            orderedBoardMember.image = image;
            orderedBoardMember.name = name;
            orderedBoardMember.namePrefix = namePrefix;
            orderedBoardMember.paymentType = paymentType;
            orderedBoardMember.role = role;
            orderedBoardMember.roleHref = roleHref;
            return orderedBoardMember;
        }

    }

}
