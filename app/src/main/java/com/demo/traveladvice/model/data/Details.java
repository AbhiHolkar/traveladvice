package com.demo.traveladvice.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Details {

    @Expose
    private String acronym;
    @Expose
    private String body;
    @Expose
    private String brand;
    @SerializedName("email_signup_link")
    private String emailSignupLink;
    @SerializedName("foi_exempt")
    private Boolean foiExempt;
    @SerializedName("important_board_members")
    private Long importantBoardMembers;
    @SerializedName("internal_name")
    private String internalName;
    @Expose
    private Logo logo;
    @SerializedName("max_cache_time")
    private Long maxCacheTime;
    @SerializedName("notes_for_editors")
    private String notesForEditors;
    @SerializedName("ordered_board_members")
    private List<OrderedBoardMember> orderedBoardMembers;
    @SerializedName("ordered_chief_professional_officers")
    private List<Object> orderedChiefProfessionalOfficers;
    @SerializedName("ordered_corporate_information_pages")
    private List<OrderedCorporateInformationPage> orderedCorporateInformationPages;
    @SerializedName("ordered_featured_documents")
    private List<OrderedFeaturedDocument> orderedFeaturedDocuments;
    @SerializedName("ordered_featured_links")
    private List<OrderedFeaturedLink> orderedFeaturedLinks;
    @SerializedName("ordered_military_personnel")
    private List<Object> orderedMilitaryPersonnel;
    @SerializedName("ordered_ministers")
    private List<OrderedMinister> orderedMinisters;
    @SerializedName("ordered_promotional_features")
    private List<Object> orderedPromotionalFeatures;
    @SerializedName("ordered_special_representatives")
    private List<OrderedSpecialRepresentative> orderedSpecialRepresentatives;
    @SerializedName("ordered_traffic_commissioners")
    private List<Object> orderedTrafficCommissioners;
    @SerializedName("organisation_featuring_priority")
    private String organisationFeaturingPriority;
    @SerializedName("organisation_govuk_status")
    private OrganisationGovukStatus organisationGovukStatus;
    @SerializedName("organisation_type")
    private String organisationType;
    @SerializedName("secondary_corporate_information_pages")
    private String secondaryCorporateInformationPages;
    @SerializedName("social_media_links")
    private List<SocialMediaLink> socialMediaLinks;
    @SerializedName("step_by_step_nav")
    private StepByStepNav stepByStepNav;
    @SerializedName("visible_to_departmental_editors")
    private Boolean visibleToDepartmentalEditors;

    public String getAcronym() {
        return acronym;
    }

    public String getBody() {
        return body;
    }

    public String getBrand() {
        return brand;
    }

    public String getEmailSignupLink() {
        return emailSignupLink;
    }

    public Boolean getFoiExempt() {
        return foiExempt;
    }

    public Long getImportantBoardMembers() {
        return importantBoardMembers;
    }

    public String getInternalName() {
        return internalName;
    }

    public Logo getLogo() {
        return logo;
    }

    public Long getMaxCacheTime() {
        return maxCacheTime;
    }

    public String getNotesForEditors() {
        return notesForEditors;
    }

    public List<OrderedBoardMember> getOrderedBoardMembers() {
        return orderedBoardMembers;
    }

    public List<Object> getOrderedChiefProfessionalOfficers() {
        return orderedChiefProfessionalOfficers;
    }

    public List<OrderedCorporateInformationPage> getOrderedCorporateInformationPages() {
        return orderedCorporateInformationPages;
    }

    public List<OrderedFeaturedDocument> getOrderedFeaturedDocuments() {
        return orderedFeaturedDocuments;
    }

    public List<OrderedFeaturedLink> getOrderedFeaturedLinks() {
        return orderedFeaturedLinks;
    }

    public List<Object> getOrderedMilitaryPersonnel() {
        return orderedMilitaryPersonnel;
    }

    public List<OrderedMinister> getOrderedMinisters() {
        return orderedMinisters;
    }

    public List<Object> getOrderedPromotionalFeatures() {
        return orderedPromotionalFeatures;
    }

    public List<OrderedSpecialRepresentative> getOrderedSpecialRepresentatives() {
        return orderedSpecialRepresentatives;
    }

    public List<Object> getOrderedTrafficCommissioners() {
        return orderedTrafficCommissioners;
    }

    public String getOrganisationFeaturingPriority() {
        return organisationFeaturingPriority;
    }

    public OrganisationGovukStatus getOrganisationGovukStatus() {
        return organisationGovukStatus;
    }

    public String getOrganisationType() {
        return organisationType;
    }

    public String getSecondaryCorporateInformationPages() {
        return secondaryCorporateInformationPages;
    }

    public List<SocialMediaLink> getSocialMediaLinks() {
        return socialMediaLinks;
    }

    public StepByStepNav getStepByStepNav() {
        return stepByStepNav;
    }

    public Boolean getVisibleToDepartmentalEditors() {
        return visibleToDepartmentalEditors;
    }

    public static class Builder {

        private String acronym;
        private String body;
        private String brand;
        private String emailSignupLink;
        private Boolean foiExempt;
        private Long importantBoardMembers;
        private String internalName;
        private Logo logo;
        private Long maxCacheTime;
        private String notesForEditors;
        private List<OrderedBoardMember> orderedBoardMembers;
        private List<Object> orderedChiefProfessionalOfficers;
        private List<OrderedCorporateInformationPage> orderedCorporateInformationPages;
        private List<OrderedFeaturedDocument> orderedFeaturedDocuments;
        private List<OrderedFeaturedLink> orderedFeaturedLinks;
        private List<Object> orderedMilitaryPersonnel;
        private List<OrderedMinister> orderedMinisters;
        private List<Object> orderedPromotionalFeatures;
        private List<OrderedSpecialRepresentative> orderedSpecialRepresentatives;
        private List<Object> orderedTrafficCommissioners;
        private String organisationFeaturingPriority;
        private OrganisationGovukStatus organisationGovukStatus;
        private String organisationType;
        private String secondaryCorporateInformationPages;
        private List<SocialMediaLink> socialMediaLinks;
        private StepByStepNav stepByStepNav;
        private Boolean visibleToDepartmentalEditors;

        public Details.Builder withAcronym(String acronym) {
            this.acronym = acronym;
            return this;
        }

        public Details.Builder withBody(String body) {
            this.body = body;
            return this;
        }

        public Details.Builder withBrand(String brand) {
            this.brand = brand;
            return this;
        }

        public Details.Builder withEmailSignupLink(String emailSignupLink) {
            this.emailSignupLink = emailSignupLink;
            return this;
        }

        public Details.Builder withFoiExempt(Boolean foiExempt) {
            this.foiExempt = foiExempt;
            return this;
        }

        public Details.Builder withImportantBoardMembers(Long importantBoardMembers) {
            this.importantBoardMembers = importantBoardMembers;
            return this;
        }

        public Details.Builder withInternalName(String internalName) {
            this.internalName = internalName;
            return this;
        }

        public Details.Builder withLogo(Logo logo) {
            this.logo = logo;
            return this;
        }

        public Details.Builder withMaxCacheTime(Long maxCacheTime) {
            this.maxCacheTime = maxCacheTime;
            return this;
        }

        public Details.Builder withNotesForEditors(String notesForEditors) {
            this.notesForEditors = notesForEditors;
            return this;
        }

        public Details.Builder withOrderedBoardMembers(List<OrderedBoardMember> orderedBoardMembers) {
            this.orderedBoardMembers = orderedBoardMembers;
            return this;
        }

        public Details.Builder withOrderedChiefProfessionalOfficers(List<Object> orderedChiefProfessionalOfficers) {
            this.orderedChiefProfessionalOfficers = orderedChiefProfessionalOfficers;
            return this;
        }

        public Details.Builder withOrderedCorporateInformationPages(List<OrderedCorporateInformationPage> orderedCorporateInformationPages) {
            this.orderedCorporateInformationPages = orderedCorporateInformationPages;
            return this;
        }

        public Details.Builder withOrderedFeaturedDocuments(List<OrderedFeaturedDocument> orderedFeaturedDocuments) {
            this.orderedFeaturedDocuments = orderedFeaturedDocuments;
            return this;
        }

        public Details.Builder withOrderedFeaturedLinks(List<OrderedFeaturedLink> orderedFeaturedLinks) {
            this.orderedFeaturedLinks = orderedFeaturedLinks;
            return this;
        }

        public Details.Builder withOrderedMilitaryPersonnel(List<Object> orderedMilitaryPersonnel) {
            this.orderedMilitaryPersonnel = orderedMilitaryPersonnel;
            return this;
        }

        public Details.Builder withOrderedMinisters(List<OrderedMinister> orderedMinisters) {
            this.orderedMinisters = orderedMinisters;
            return this;
        }

        public Details.Builder withOrderedPromotionalFeatures(List<Object> orderedPromotionalFeatures) {
            this.orderedPromotionalFeatures = orderedPromotionalFeatures;
            return this;
        }

        public Details.Builder withOrderedSpecialRepresentatives(List<OrderedSpecialRepresentative> orderedSpecialRepresentatives) {
            this.orderedSpecialRepresentatives = orderedSpecialRepresentatives;
            return this;
        }

        public Details.Builder withOrderedTrafficCommissioners(List<Object> orderedTrafficCommissioners) {
            this.orderedTrafficCommissioners = orderedTrafficCommissioners;
            return this;
        }

        public Details.Builder withOrganisationFeaturingPriority(String organisationFeaturingPriority) {
            this.organisationFeaturingPriority = organisationFeaturingPriority;
            return this;
        }

        public Details.Builder withOrganisationGovukStatus(OrganisationGovukStatus organisationGovukStatus) {
            this.organisationGovukStatus = organisationGovukStatus;
            return this;
        }

        public Details.Builder withOrganisationType(String organisationType) {
            this.organisationType = organisationType;
            return this;
        }

        public Details.Builder withSecondaryCorporateInformationPages(String secondaryCorporateInformationPages) {
            this.secondaryCorporateInformationPages = secondaryCorporateInformationPages;
            return this;
        }

        public Details.Builder withSocialMediaLinks(List<SocialMediaLink> socialMediaLinks) {
            this.socialMediaLinks = socialMediaLinks;
            return this;
        }

        public Details.Builder withStepByStepNav(StepByStepNav stepByStepNav) {
            this.stepByStepNav = stepByStepNav;
            return this;
        }

        public Details.Builder withVisibleToDepartmentalEditors(Boolean visibleToDepartmentalEditors) {
            this.visibleToDepartmentalEditors = visibleToDepartmentalEditors;
            return this;
        }

        public Details build() {
            Details details = new Details();
            details.acronym = acronym;
            details.body = body;
            details.brand = brand;
            details.emailSignupLink = emailSignupLink;
            details.foiExempt = foiExempt;
            details.importantBoardMembers = importantBoardMembers;
            details.internalName = internalName;
            details.logo = logo;
            details.maxCacheTime = maxCacheTime;
            details.notesForEditors = notesForEditors;
            details.orderedBoardMembers = orderedBoardMembers;
            details.orderedChiefProfessionalOfficers = orderedChiefProfessionalOfficers;
            details.orderedCorporateInformationPages = orderedCorporateInformationPages;
            details.orderedFeaturedDocuments = orderedFeaturedDocuments;
            details.orderedFeaturedLinks = orderedFeaturedLinks;
            details.orderedMilitaryPersonnel = orderedMilitaryPersonnel;
            details.orderedMinisters = orderedMinisters;
            details.orderedPromotionalFeatures = orderedPromotionalFeatures;
            details.orderedSpecialRepresentatives = orderedSpecialRepresentatives;
            details.orderedTrafficCommissioners = orderedTrafficCommissioners;
            details.organisationFeaturingPriority = organisationFeaturingPriority;
            details.organisationGovukStatus = organisationGovukStatus;
            details.organisationType = organisationType;
            details.secondaryCorporateInformationPages = secondaryCorporateInformationPages;
            details.socialMediaLinks = socialMediaLinks;
            details.stepByStepNav = stepByStepNav;
            details.visibleToDepartmentalEditors = visibleToDepartmentalEditors;
            return details;
        }

    }

}
