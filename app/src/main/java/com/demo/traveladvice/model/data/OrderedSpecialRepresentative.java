package com.demo.traveladvice.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class OrderedSpecialRepresentative {

    @SerializedName("attends_cabinet_type")
    private Object attendsCabinetType;
    @Expose
    private String href;
    @Expose
    private Image image;
    @Expose
    private String name;
    @SerializedName("name_prefix")
    private Object namePrefix;
    @SerializedName("payment_type")
    private Object paymentType;
    @Expose
    private String role;
    @SerializedName("role_href")
    private Object roleHref;

    public Object getAttendsCabinetType() {
        return attendsCabinetType;
    }

    public String getHref() {
        return href;
    }

    public Image getImage() {
        return image;
    }

    public String getName() {
        return name;
    }

    public Object getNamePrefix() {
        return namePrefix;
    }

    public Object getPaymentType() {
        return paymentType;
    }

    public String getRole() {
        return role;
    }

    public Object getRoleHref() {
        return roleHref;
    }

    public static class Builder {

        private Object attendsCabinetType;
        private String href;
        private Image image;
        private String name;
        private Object namePrefix;
        private Object paymentType;
        private String role;
        private Object roleHref;

        public OrderedSpecialRepresentative.Builder withAttendsCabinetType(Object attendsCabinetType) {
            this.attendsCabinetType = attendsCabinetType;
            return this;
        }

        public OrderedSpecialRepresentative.Builder withHref(String href) {
            this.href = href;
            return this;
        }

        public OrderedSpecialRepresentative.Builder withImage(Image image) {
            this.image = image;
            return this;
        }

        public OrderedSpecialRepresentative.Builder withName(String name) {
            this.name = name;
            return this;
        }

        public OrderedSpecialRepresentative.Builder withNamePrefix(Object namePrefix) {
            this.namePrefix = namePrefix;
            return this;
        }

        public OrderedSpecialRepresentative.Builder withPaymentType(Object paymentType) {
            this.paymentType = paymentType;
            return this;
        }

        public OrderedSpecialRepresentative.Builder withRole(String role) {
            this.role = role;
            return this;
        }

        public OrderedSpecialRepresentative.Builder withRoleHref(Object roleHref) {
            this.roleHref = roleHref;
            return this;
        }

        public OrderedSpecialRepresentative build() {
            OrderedSpecialRepresentative orderedSpecialRepresentative = new OrderedSpecialRepresentative();
            orderedSpecialRepresentative.attendsCabinetType = attendsCabinetType;
            orderedSpecialRepresentative.href = href;
            orderedSpecialRepresentative.image = image;
            orderedSpecialRepresentative.name = name;
            orderedSpecialRepresentative.namePrefix = namePrefix;
            orderedSpecialRepresentative.paymentType = paymentType;
            orderedSpecialRepresentative.role = role;
            orderedSpecialRepresentative.roleHref = roleHref;
            return orderedSpecialRepresentative;
        }

    }

}
