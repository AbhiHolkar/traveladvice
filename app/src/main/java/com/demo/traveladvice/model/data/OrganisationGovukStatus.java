package com.demo.traveladvice.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class OrganisationGovukStatus {

    @Expose
    private String status;
    @SerializedName("updated_at")
    private Object updatedAt;
    @Expose
    private Object url;

    public String getStatus() {
        return status;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public Object getUrl() {
        return url;
    }

    public static class Builder {

        private String status;
        private Object updatedAt;
        private Object url;

        public OrganisationGovukStatus.Builder withStatus(String status) {
            this.status = status;
            return this;
        }

        public OrganisationGovukStatus.Builder withUpdatedAt(Object updatedAt) {
            this.updatedAt = updatedAt;
            return this;
        }

        public OrganisationGovukStatus.Builder withUrl(Object url) {
            this.url = url;
            return this;
        }

        public OrganisationGovukStatus build() {
            OrganisationGovukStatus organisationGovukStatus = new OrganisationGovukStatus();
            organisationGovukStatus.status = status;
            organisationGovukStatus.updatedAt = updatedAt;
            organisationGovukStatus.url = url;
            return organisationGovukStatus;
        }

    }

}
