package com.demo.traveladvice.model.data;

import com.google.gson.annotations.Expose;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class OrderedCorporateInformationPage {

    @Expose
    private String href;
    @Expose
    private String title;

    public String getHref() {
        return href;
    }

    public String getTitle() {
        return title;
    }

    public static class Builder {

        private String href;
        private String title;

        public OrderedCorporateInformationPage.Builder withHref(String href) {
            this.href = href;
            return this;
        }

        public OrderedCorporateInformationPage.Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public OrderedCorporateInformationPage build() {
            OrderedCorporateInformationPage orderedCorporateInformationPage = new OrderedCorporateInformationPage();
            orderedCorporateInformationPage.href = href;
            orderedCorporateInformationPage.title = title;
            return orderedCorporateInformationPage;
        }

    }

}
