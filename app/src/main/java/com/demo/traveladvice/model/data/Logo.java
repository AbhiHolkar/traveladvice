package com.demo.traveladvice.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Logo {

    @Expose
    private String crest;
    @SerializedName("formatted_title")
    private String formattedTitle;

    public String getCrest() {
        return crest;
    }

    public String getFormattedTitle() {
        return formattedTitle;
    }

    public static class Builder {

        private String crest;
        private String formattedTitle;

        public Logo.Builder withCrest(String crest) {
            this.crest = crest;
            return this;
        }

        public Logo.Builder withFormattedTitle(String formattedTitle) {
            this.formattedTitle = formattedTitle;
            return this;
        }

        public Logo build() {
            Logo logo = new Logo();
            logo.crest = crest;
            logo.formattedTitle = formattedTitle;
            return logo;
        }

    }

}
