package com.demo.traveladvice.model.data;

import com.google.gson.annotations.Expose;

import java.util.List;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Country {

    @Expose
    private String name;
    @Expose
    private String slug;
    @Expose
    private List<Object> synonyms;

    public String getName() {
        return name;
    }

    public String getSlug() {
        return slug;
    }

    public List<Object> getSynonyms() {
        return synonyms;
    }

    public static class Builder {

        private String name;
        private String slug;
        private List<Object> synonyms;

        public Country.Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Country.Builder withSlug(String slug) {
            this.slug = slug;
            return this;
        }

        public Country.Builder withSynonyms(List<Object> synonyms) {
            this.synonyms = synonyms;
            return this;
        }

        public Country build() {
            Country country = new Country();
            country.name = name;
            country.slug = slug;
            country.synonyms = synonyms;
            return country;
        }

    }

}
