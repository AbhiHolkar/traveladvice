package com.demo.traveladvice.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ParentTaxon {

    @SerializedName("api_path")
    private String apiPath;
    @SerializedName("api_url")
    private String apiUrl;
    @SerializedName("base_path")
    private String basePath;
    @SerializedName("content_id")
    private String contentId;
    @Expose
    private Details details;
    @SerializedName("document_type")
    private String documentType;
    @Expose
    private Links links;
    @Expose
    private String locale;
    @Expose
    private String phase;
    @SerializedName("public_updated_at")
    private String publicUpdatedAt;
    @SerializedName("schema_name")
    private String schemaName;
    @Expose
    private String title;
    @SerializedName("web_url")
    private String webUrl;
    @Expose
    private Boolean withdrawn;

    public String getApiPath() {
        return apiPath;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public String getBasePath() {
        return basePath;
    }

    public String getContentId() {
        return contentId;
    }

    public Details getDetails() {
        return details;
    }

    public String getDocumentType() {
        return documentType;
    }

    public Links getLinks() {
        return links;
    }

    public String getLocale() {
        return locale;
    }

    public String getPhase() {
        return phase;
    }

    public String getPublicUpdatedAt() {
        return publicUpdatedAt;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public String getTitle() {
        return title;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public Boolean getWithdrawn() {
        return withdrawn;
    }

    public static class Builder {

        private String apiPath;
        private String apiUrl;
        private String basePath;
        private String contentId;
        private Details details;
        private String documentType;
        private Links links;
        private String locale;
        private String phase;
        private String publicUpdatedAt;
        private String schemaName;
        private String title;
        private String webUrl;
        private Boolean withdrawn;

        public ParentTaxon.Builder withApiPath(String apiPath) {
            this.apiPath = apiPath;
            return this;
        }

        public ParentTaxon.Builder withApiUrl(String apiUrl) {
            this.apiUrl = apiUrl;
            return this;
        }

        public ParentTaxon.Builder withBasePath(String basePath) {
            this.basePath = basePath;
            return this;
        }

        public ParentTaxon.Builder withContentId(String contentId) {
            this.contentId = contentId;
            return this;
        }

        public ParentTaxon.Builder withDetails(Details details) {
            this.details = details;
            return this;
        }

        public ParentTaxon.Builder withDocumentType(String documentType) {
            this.documentType = documentType;
            return this;
        }

        public ParentTaxon.Builder withLinks(Links links) {
            this.links = links;
            return this;
        }

        public ParentTaxon.Builder withLocale(String locale) {
            this.locale = locale;
            return this;
        }

        public ParentTaxon.Builder withPhase(String phase) {
            this.phase = phase;
            return this;
        }

        public ParentTaxon.Builder withPublicUpdatedAt(String publicUpdatedAt) {
            this.publicUpdatedAt = publicUpdatedAt;
            return this;
        }

        public ParentTaxon.Builder withSchemaName(String schemaName) {
            this.schemaName = schemaName;
            return this;
        }

        public ParentTaxon.Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public ParentTaxon.Builder withWebUrl(String webUrl) {
            this.webUrl = webUrl;
            return this;
        }

        public ParentTaxon.Builder withWithdrawn(Boolean withdrawn) {
            this.withdrawn = withdrawn;
            return this;
        }

        public ParentTaxon build() {
            ParentTaxon parentTaxon = new ParentTaxon();
            parentTaxon.apiPath = apiPath;
            parentTaxon.apiUrl = apiUrl;
            parentTaxon.basePath = basePath;
            parentTaxon.contentId = contentId;
            parentTaxon.details = details;
            parentTaxon.documentType = documentType;
            parentTaxon.links = links;
            parentTaxon.locale = locale;
            parentTaxon.phase = phase;
            parentTaxon.publicUpdatedAt = publicUpdatedAt;
            parentTaxon.schemaName = schemaName;
            parentTaxon.title = title;
            parentTaxon.webUrl = webUrl;
            parentTaxon.withdrawn = withdrawn;
            return parentTaxon;
        }

    }

}
