package com.demo.traveladvice.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class RootTaxon {

    @SerializedName("api_path")
    private String apiPath;
    @SerializedName("api_url")
    private String apiUrl;
    @SerializedName("base_path")
    private String basePath;
    @SerializedName("content_id")
    private String contentId;
    @Expose
    private String description;
    @SerializedName("document_type")
    private String documentType;
    @Expose
    private Links links;
    @Expose
    private String locale;
    @SerializedName("public_updated_at")
    private String publicUpdatedAt;
    @SerializedName("schema_name")
    private String schemaName;
    @Expose
    private String title;
    @SerializedName("web_url")
    private String webUrl;
    @Expose
    private Boolean withdrawn;

    public String getApiPath() {
        return apiPath;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public String getBasePath() {
        return basePath;
    }

    public String getContentId() {
        return contentId;
    }

    public String getDescription() {
        return description;
    }

    public String getDocumentType() {
        return documentType;
    }

    public Links getLinks() {
        return links;
    }

    public String getLocale() {
        return locale;
    }

    public String getPublicUpdatedAt() {
        return publicUpdatedAt;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public String getTitle() {
        return title;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public Boolean getWithdrawn() {
        return withdrawn;
    }

    public static class Builder {

        private String apiPath;
        private String apiUrl;
        private String basePath;
        private String contentId;
        private String description;
        private String documentType;
        private Links links;
        private String locale;
        private String publicUpdatedAt;
        private String schemaName;
        private String title;
        private String webUrl;
        private Boolean withdrawn;

        public RootTaxon.Builder withApiPath(String apiPath) {
            this.apiPath = apiPath;
            return this;
        }

        public RootTaxon.Builder withApiUrl(String apiUrl) {
            this.apiUrl = apiUrl;
            return this;
        }

        public RootTaxon.Builder withBasePath(String basePath) {
            this.basePath = basePath;
            return this;
        }

        public RootTaxon.Builder withContentId(String contentId) {
            this.contentId = contentId;
            return this;
        }

        public RootTaxon.Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public RootTaxon.Builder withDocumentType(String documentType) {
            this.documentType = documentType;
            return this;
        }

        public RootTaxon.Builder withLinks(Links links) {
            this.links = links;
            return this;
        }

        public RootTaxon.Builder withLocale(String locale) {
            this.locale = locale;
            return this;
        }

        public RootTaxon.Builder withPublicUpdatedAt(String publicUpdatedAt) {
            this.publicUpdatedAt = publicUpdatedAt;
            return this;
        }

        public RootTaxon.Builder withSchemaName(String schemaName) {
            this.schemaName = schemaName;
            return this;
        }

        public RootTaxon.Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public RootTaxon.Builder withWebUrl(String webUrl) {
            this.webUrl = webUrl;
            return this;
        }

        public RootTaxon.Builder withWithdrawn(Boolean withdrawn) {
            this.withdrawn = withdrawn;
            return this;
        }

        public RootTaxon build() {
            RootTaxon rootTaxon = new RootTaxon();
            rootTaxon.apiPath = apiPath;
            rootTaxon.apiUrl = apiUrl;
            rootTaxon.basePath = basePath;
            rootTaxon.contentId = contentId;
            rootTaxon.description = description;
            rootTaxon.documentType = documentType;
            rootTaxon.links = links;
            rootTaxon.locale = locale;
            rootTaxon.publicUpdatedAt = publicUpdatedAt;
            rootTaxon.schemaName = schemaName;
            rootTaxon.title = title;
            rootTaxon.webUrl = webUrl;
            rootTaxon.withdrawn = withdrawn;
            return rootTaxon;
        }

    }

}
