package com.demo.traveladvice.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class OrderedRelatedItem {

    @SerializedName("api_path")
    private String apiPath;
    @SerializedName("api_url")
    private String apiUrl;
    @SerializedName("base_path")
    private String basePath;
    @SerializedName("content_id")
    private String contentId;
    @Expose
    private String description;
    @SerializedName("document_type")
    private String documentType;
    @Expose
    private Links links;
    @Expose
    private String locale;
    @SerializedName("public_updated_at")
    private String publicUpdatedAt;
    @SerializedName("schema_name")
    private String schemaName;
    @Expose
    private String title;
    @SerializedName("web_url")
    private String webUrl;
    @Expose
    private Boolean withdrawn;

    public String getApiPath() {
        return apiPath;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public String getBasePath() {
        return basePath;
    }

    public String getContentId() {
        return contentId;
    }

    public String getDescription() {
        return description;
    }

    public String getDocumentType() {
        return documentType;
    }

    public Links getLinks() {
        return links;
    }

    public String getLocale() {
        return locale;
    }

    public String getPublicUpdatedAt() {
        return publicUpdatedAt;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public String getTitle() {
        return title;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public Boolean getWithdrawn() {
        return withdrawn;
    }

    public static class Builder {

        private String apiPath;
        private String apiUrl;
        private String basePath;
        private String contentId;
        private String description;
        private String documentType;
        private Links links;
        private String locale;
        private String publicUpdatedAt;
        private String schemaName;
        private String title;
        private String webUrl;
        private Boolean withdrawn;

        public OrderedRelatedItem.Builder withApiPath(String apiPath) {
            this.apiPath = apiPath;
            return this;
        }

        public OrderedRelatedItem.Builder withApiUrl(String apiUrl) {
            this.apiUrl = apiUrl;
            return this;
        }

        public OrderedRelatedItem.Builder withBasePath(String basePath) {
            this.basePath = basePath;
            return this;
        }

        public OrderedRelatedItem.Builder withContentId(String contentId) {
            this.contentId = contentId;
            return this;
        }

        public OrderedRelatedItem.Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public OrderedRelatedItem.Builder withDocumentType(String documentType) {
            this.documentType = documentType;
            return this;
        }

        public OrderedRelatedItem.Builder withLinks(Links links) {
            this.links = links;
            return this;
        }

        public OrderedRelatedItem.Builder withLocale(String locale) {
            this.locale = locale;
            return this;
        }

        public OrderedRelatedItem.Builder withPublicUpdatedAt(String publicUpdatedAt) {
            this.publicUpdatedAt = publicUpdatedAt;
            return this;
        }

        public OrderedRelatedItem.Builder withSchemaName(String schemaName) {
            this.schemaName = schemaName;
            return this;
        }

        public OrderedRelatedItem.Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public OrderedRelatedItem.Builder withWebUrl(String webUrl) {
            this.webUrl = webUrl;
            return this;
        }

        public OrderedRelatedItem.Builder withWithdrawn(Boolean withdrawn) {
            this.withdrawn = withdrawn;
            return this;
        }

        public OrderedRelatedItem build() {
            OrderedRelatedItem orderedRelatedItem = new OrderedRelatedItem();
            orderedRelatedItem.apiPath = apiPath;
            orderedRelatedItem.apiUrl = apiUrl;
            orderedRelatedItem.basePath = basePath;
            orderedRelatedItem.contentId = contentId;
            orderedRelatedItem.description = description;
            orderedRelatedItem.documentType = documentType;
            orderedRelatedItem.links = links;
            orderedRelatedItem.locale = locale;
            orderedRelatedItem.publicUpdatedAt = publicUpdatedAt;
            orderedRelatedItem.schemaName = schemaName;
            orderedRelatedItem.title = title;
            orderedRelatedItem.webUrl = webUrl;
            orderedRelatedItem.withdrawn = withdrawn;
            return orderedRelatedItem;
        }

    }

}
