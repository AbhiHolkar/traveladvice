package com.demo.traveladvice.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Child {

    @SerializedName("api_path")
    private String apiPath;
    @SerializedName("api_url")
    private String apiUrl;
    @SerializedName("base_path")
    private String basePath;
    @SerializedName("change_description")
    private String changeDescription;
    @SerializedName("content_id")
    private String contentId;
    @Expose
    private Country country;
    @Expose
    private String description;
    @SerializedName("document_type")
    private String documentType;
    @Expose
    private Links links;
    @Expose
    private String locale;
    @SerializedName("public_updated_at")
    private String publicUpdatedAt;
    @SerializedName("schema_name")
    private String schemaName;
    @Expose
    private String title;
    @SerializedName("web_url")
    private String webUrl;
    @Expose
    private Boolean withdrawn;

    public String getApiPath() {
        return apiPath;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public String getBasePath() {
        return basePath;
    }

    public String getChangeDescription() {
        return changeDescription;
    }

    public String getContentId() {
        return contentId;
    }

    public Country getCountry() {
        return country;
    }

    public String getDescription() {
        return description;
    }

    public String getDocumentType() {
        return documentType;
    }

    public Links getLinks() {
        return links;
    }

    public String getLocale() {
        return locale;
    }

    public String getPublicUpdatedAt() {
        return publicUpdatedAt;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public String getTitle() {
        return title;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public Boolean getWithdrawn() {
        return withdrawn;
    }

    public static class Builder {

        private String apiPath;
        private String apiUrl;
        private String basePath;
        private String changeDescription;
        private String contentId;
        private Country country;
        private String description;
        private String documentType;
        private Links links;
        private String locale;
        private String publicUpdatedAt;
        private String schemaName;
        private String title;
        private String webUrl;
        private Boolean withdrawn;

        public Child.Builder withApiPath(String apiPath) {
            this.apiPath = apiPath;
            return this;
        }

        public Child.Builder withApiUrl(String apiUrl) {
            this.apiUrl = apiUrl;
            return this;
        }

        public Child.Builder withBasePath(String basePath) {
            this.basePath = basePath;
            return this;
        }

        public Child.Builder withChangeDescription(String changeDescription) {
            this.changeDescription = changeDescription;
            return this;
        }

        public Child.Builder withContentId(String contentId) {
            this.contentId = contentId;
            return this;
        }

        public Child.Builder withCountry(Country country) {
            this.country = country;
            return this;
        }

        public Child.Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Child.Builder withDocumentType(String documentType) {
            this.documentType = documentType;
            return this;
        }

        public Child.Builder withLinks(Links links) {
            this.links = links;
            return this;
        }

        public Child.Builder withLocale(String locale) {
            this.locale = locale;
            return this;
        }

        public Child.Builder withPublicUpdatedAt(String publicUpdatedAt) {
            this.publicUpdatedAt = publicUpdatedAt;
            return this;
        }

        public Child.Builder withSchemaName(String schemaName) {
            this.schemaName = schemaName;
            return this;
        }

        public Child.Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public Child.Builder withWebUrl(String webUrl) {
            this.webUrl = webUrl;
            return this;
        }

        public Child.Builder withWithdrawn(Boolean withdrawn) {
            this.withdrawn = withdrawn;
            return this;
        }

        public Child build() {
            Child child = new Child();
            child.apiPath = apiPath;
            child.apiUrl = apiUrl;
            child.basePath = basePath;
            child.changeDescription = changeDescription;
            child.contentId = contentId;
            child.country = country;
            child.description = description;
            child.documentType = documentType;
            child.links = links;
            child.locale = locale;
            child.publicUpdatedAt = publicUpdatedAt;
            child.schemaName = schemaName;
            child.title = title;
            child.webUrl = webUrl;
            child.withdrawn = withdrawn;
            return child;
        }

    }

}
