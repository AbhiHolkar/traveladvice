package com.demo.traveladvice.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Introduction {

    @Expose
    private String content;
    @SerializedName("content_type")
    private String contentType;

    public String getContent() {
        return content;
    }

    public String getContentType() {
        return contentType;
    }

    public static class Builder {

        private String content;
        private String contentType;

        public Introduction.Builder withContent(String content) {
            this.content = content;
            return this;
        }

        public Introduction.Builder withContentType(String contentType) {
            this.contentType = contentType;
            return this;
        }

        public Introduction build() {
            Introduction introduction = new Introduction();
            introduction.content = content;
            introduction.contentType = contentType;
            return introduction;
        }

    }

}
