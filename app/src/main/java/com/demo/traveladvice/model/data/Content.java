package com.demo.traveladvice.model.data;

import com.google.gson.annotations.Expose;

import java.util.List;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Content {

    @Expose
    private List<Content> contents;
    @Expose
    private String href;
    @Expose
    private String text;
    @Expose
    private String type;

    public List<Content> getContents() {
        return contents;
    }

    public String getHref() {
        return href;
    }

    public String getText() {
        return text;
    }

    public String getType() {
        return type;
    }

    public static class Builder {

        private List<Content> contents;
        private String href;
        private String text;
        private String type;

        public Content.Builder withContents(List<Content> contents) {
            this.contents = contents;
            return this;
        }

        public Content.Builder withHref(String href) {
            this.href = href;
            return this;
        }

        public Content.Builder withText(String text) {
            this.text = text;
            return this;
        }

        public Content.Builder withType(String type) {
            this.type = type;
            return this;
        }

        public Content build() {
            Content content = new Content();
            content.contents = contents;
            content.href = href;
            content.text = text;
            content.type = type;
            return content;
        }

    }

}
