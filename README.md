# TravelAdvice

TravelAdvice application fetches travel advise for different countries .
The advise is available as REST API at :https://www.gov.uk/api/content/foreign-travel-advice.
Please follow the guide lines in repo.

# Application Architecture.
- Application uses MVVVM architecture & CLEAN architecture & principles of SOLID programming.
- Dagger 2 is used for Dependency Injection and ButterKnife for View Injections.
- Retrofit is used for Network layer.
- RxJava2 is used for Background/Asychronous operations like Network call/ response transformation.
- GSON library is used for parsing of data
- Application uses Mockito for Junit test cases.
- Repository and Use case concept of Clean architecture makes code modular and testable.
- Recylcer view is used to display list of countries

# Note
- JSON to POJO construction done by JetBrains plugin. Generally used JSONPojoSchema(http://www.jsonschema2pojo.org/) didn't work due response data limit.
  The concern plugin added redundant builders on POJO construtcion.

# Pre-Requiste
- Android studio . Java 1.8

# Run/Test
- App has been tested on device S5 and emulator Nexus API 23
- App unit test covers Viewmodel and Usecase.

# Techincal Debt
- Wrap CountryModel and Response inside a Generic App Response and send it downstream
- Add persistence to save response using any DB(Realm/ORM/SQLite)
- Add more test coverage.
- UI code on fragments
- Filter redundant API response due to POJO construction.

# Known bug
- Webview cangoBack and canGoForward not implemented.
- TBD persistence


